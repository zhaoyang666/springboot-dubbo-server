package org.spring.springboot.dubbo;

public interface TestDubboService {
    /**
     * 求平方
     */
    String getSquare(int i);
}
