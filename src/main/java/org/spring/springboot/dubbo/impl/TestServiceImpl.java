package org.spring.springboot.dubbo.impl;

import com.alibaba.dubbo.config.annotation.Service;
import org.spring.springboot.dubbo.TestDubboService;

//  注册为 Dubbo 服务
@Service(version = "1.0.0")
public class TestServiceImpl implements TestDubboService {
    public String getSquare(int i) {
        System.out.println("服务器方法被调用。。消费者param为："+i);
        String result=i*i+"";
        return result;
    }
}
